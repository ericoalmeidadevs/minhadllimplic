unit Main.View;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, FMX.Objects,
  FMX.ListBox, FMX.Controls.Presentation, FMX.StdCtrls, FMX.ScrollBox, FMX.Memo,
  FMX.Edit;

type
  TForm1 = class(TForm)
    Image1: TImage;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
  private
    { Private declarations }
  public

  end;

function Somar(AValueOne: currency; AValueTwo: currency): currency; stdcall;
  external 'MinhaDll.dll';
function Subtrair(AValueOne: currency; AValueTwo: currency): currency; stdcall;
  external 'MinhaDll.dll';
function Multiplicar(AValueOne: currency; AValueTwo: currency): currency; stdcall;
  external 'MinhaDll.dll';
function Dividir(AValueOne: currency; AValueTwo: currency): currency; stdcall;
  external 'MinhaDll.dll';
function Desconto(AValue: currency; ADesconto: currency): currency; stdcall;
  external 'MinhaDll.dll';
function DescontoConcedido(AValue: currency; ADesconto: currency): currency; stdcall;
  external 'MinhaDll.dll';

var
  Form1: TForm1;

implementation

{$R *.fmx}

procedure TForm1.Button1Click(Sender: TObject);
begin
  Edit3.Text := CurrToStr(Somar(StrToCurr(Edit1.Text), StrToCurr(Edit2.Text)));
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Edit3.Text := CurrToStr(Subtrair(StrToCurr(Edit1.Text), StrToCurr(Edit2.Text)));
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  Edit3.Text := CurrToStr(Multiplicar(StrToCurr(Edit1.Text), StrToCurr(Edit2.Text)));
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  Edit3.Text := CurrToStr(Dividir(StrToCurr(Edit1.Text), StrToCurr(Edit2.Text)));
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  Edit3.Text := CurrToStr(Desconto(StrToCurr(Edit1.Text), StrToCurr(Edit2.Text)));
end;

procedure TForm1.Button6Click(Sender: TObject);
begin
  Edit3.Text := CurrToStr(DescontoConcedido(StrToCurr(Edit1.Text), StrToCurr(Edit2.Text)));
end;

end.
